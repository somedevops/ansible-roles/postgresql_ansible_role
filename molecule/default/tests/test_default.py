"""Role testing files using testinfra."""
import pytest

@pytest.mark.parametrize("pkgs", [
    "python3-psycopg2",
    "postgresql-14",
    "postgresql-common",
    "postgresql-client-14",
    "postgresql-client-common"
])
def test_packages(host, pkgs):
    pkg = host.package(pkgs)
    assert pkg.is_installed

def test_postgresql_service(host):
    s = host.service("postgresql")
    assert s.is_enabled
    assert s.is_running

def test_postgres_user(host):
    u = host.user("postgres")
    assert u.exists
    assert u.group == 'postgres'
    assert u.home == '/var/lib/postgresql'
    assert u.shell == '/bin/bash'

@pytest.mark.parametrize("expected_databases", [
    "db1",
    "postgres",
    "template0"
])
def test_databases_and_privileges(host, expected_databases):
    existing_databases = host.check_output("psql -U postgres -c '\l'")
    # Test databases
    for expected_database in expected_databases:
        assert expected_database in existing_databases
    # Test privileges
    assert 'user2' in existing_databases
    assert 'foo_user' not in existing_databases
    assert 'foobar_user' not in existing_databases

@pytest.mark.parametrize("expected_users", [
    "postgres",
    "someuser",
    "user2",
    "user3"

])
def test_db_users(host, expected_users):
    existing_users = host.check_output("psql -U postgres -c '\du'")
    for expected_user in expected_users:
        assert expected_user in existing_users
